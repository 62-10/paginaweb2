document.getElementById('btnTabla').addEventListener('click', function() {
    document.getElementById('inputArchivo').click();
});

document.getElementById('inputArchivo').addEventListener('change', function(event) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = function(e) {
        const imageUrl = e.target.result;
        document.getElementById('imagenMostrada').src = imageUrl;
        document.getElementById('imagenMostrada').style.display = 'block';
        document.getElementById('sinArchivosLabel').style.display = 'none'; // Oculta el primer label
        document.getElementById('mostrandoImagen').style.display = 'block'; // Muestra el segundo label después de seleccionar la imagen
    };

    reader.readAsDataURL(file);
});